<?php

use JeroenZwart\CsvSeeder\CsvSeeder;

class HousesSeeder2 extends CsvSeeder {


    public function __construct()
    {
        $this->timestamps = true;
        $this->tablename = 'houses';
        $this->delimiter = ',';
        $this->file = base_path().'/database/seeds/data/property-data.csv';
    }

    public function run()
    {
        \Illuminate\Support\Facades\DB::table($this->tablename)->truncate();

        parent::run();
    }
}
