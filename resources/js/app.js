require('./bootstrap');

import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

window.Vue = require('vue');

Vue.use(VueAxios, axios);
Vue.use(ElementUI);

axios.defaults.baseURL = `/api`;

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import IndexComponent from './components/Index.vue';
Vue.component('index', IndexComponent);

const app = new Vue({
    el: '#app'
});
