<?php

namespace App\Http\Api\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Houses\IndexRequest;
use App\Http\Resources\Houses\HouseResource;
use App\Services\House\HouseService;

class HousesController extends Controller
{

    protected $houseService;

    public function __construct(HouseService $houseService)
    {
        $this->houseService = $houseService;
    }

    public function index(IndexRequest $request)
    {
        $houses = $this->houseService->paginateHouses($request->validated());
        return HouseResource::collection($houses);
    }

    public function getMaxPrice()
    {
        $maxPrice = $this->houseService->getMaxPrice();
        return $maxPrice;
    }

}
