<?php

namespace App\Filters;

class HousesFilter extends BaseFilter
{

    public function name($value)
    {
        $this->builder->where('houses.name', 'LIKE', "%{$value}%");
    }

    public function price_from($value)
    {
        $this->builder->where('houses.price', '>=', $value);
    }

    public function price_to($value)
    {
        $this->builder->where('houses.price', '<=', $value);
    }


    public function bedrooms($value)
    {
        $this->builder->where('houses.bedrooms', $value);
    }

    public function bathrooms($value)
    {
        $this->builder->where('houses.bathrooms', $value);
    }

    public function storeys($value)
    {
        $this->builder->where('houses.storeys', $value);
    }

    public function garages($value)
    {
        $this->builder->where('houses.garages', $value);
    }

}
