<?php

namespace App\Services\House;

use App\Filters\HousesFilter;
use App\House;

class HouseService
{

    /**
     * @param array $params
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateHouses($params=[])
    {
        $filter = new HousesFilter($params);
        return House::filter($filter)->paginate(21);
    }

    /**
     * @return integer
     */
    public function getMaxPrice()
    {
        return House::max('price');
    }

}
